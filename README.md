A support layer for enabling specific configurations of testing of Xen.

To enable: add 'xentests' to DISTRO\_FEATURES.

License
-------

All metadata is MIT licensed unless otherwise stated.
