FILESEXTRAPATHS:prepend := "${THISDIR}/xen:"

# Enable PV 32-bit to allow test coverage of that interface
SRC_URI:append = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'xentests', 'file://pv32.cfg', '', d)} \
    "
