do_testimage[depends] += " xen-image-minimal:do_build"

# The default wic script for x86 depends on deployed xen
XEN_IMAGE_WIC_DEPENDS = ""
XEN_IMAGE_WIC_DEPENDS:x86-64 = "xen:do_deploy"
do_image_wic[depends] += " ${XEN_IMAGE_WIC_DEPENDS}"
